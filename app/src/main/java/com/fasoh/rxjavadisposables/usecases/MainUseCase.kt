package com.fasoh.rxjavadisposables.usecases

import com.fasoh.rxjavadisposables.AutoDisposable
import com.fasoh.rxjavadisposables.addTo
import com.fasoh.rxjavadisposables.models.Rate
import com.fasoh.rxjavadisposables.repositories.MainRepository

interface MainUseCase {
    fun execute(autoDisposable: AutoDisposable, listener: MainUseCaseListener)
}

class MainUseCaseImpl(private val mainRepository: MainRepository) : MainUseCase {
    override fun execute(autoDisposable: AutoDisposable, listener: MainUseCaseListener) {
        mainRepository.fetchRates()
            .subscribe({
                listener.onSuccess(it)
            }, {
                listener.onError(it)
            }).addTo(autoDisposable)
    }

}

interface MainUseCaseListener {
    fun onSuccess(rates: List<Rate>)
    fun onError(error: Throwable)
}