package com.fasoh.rxjavadisposables.flows

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.fasoh.rxjavadisposables.AutoDisposable
import com.fasoh.rxjavadisposables.R
import com.fasoh.rxjavadisposables.models.Rate
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModels()

    override fun onResume() {
        super.onResume()
        viewModel.setup(AutoDisposable().apply { bindTo(lifecycle) })
        viewModel.rates.observe(this, Observer {
            //If its 1 you exceeded the requests per hour.
            if (it.size > 1)
                recycler.adapter = RateAdapter(it)
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    class RateAdapter(private val items: List<Rate>) :
        RecyclerView.Adapter<RateAdapter.RateViewHolder>() {

        class RateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private val currency = itemView.findViewById<TextView>(R.id.currency)
            private val ask = itemView.findViewById<TextView>(R.id.ask)
            private val bid = itemView.findViewById<TextView>(R.id.bid)

            fun bind(rate: Rate) {
                currency.text = rate.currency
                ask.text = rate.ask
                bid.text = rate.bid
            }

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RateViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.rate_item, parent, false)
            return RateViewHolder(view)
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: RateViewHolder, position: Int) {
            holder.bind(items[position])
        }

    }


}