package com.fasoh.rxjavadisposables

import io.reactivex.rxjava3.android.plugins.RxAndroidPlugins
import io.reactivex.rxjava3.schedulers.Schedulers
import org.junit.Before

open class LifecycleAwareTest {
    protected val autoDisposable = AutoDisposable()
    protected val lifecycleOwner = TestLifecycleOwner()

    @Before
    open fun setUp() {
        autoDisposable.bindTo(lifecycleOwner.lifecycle)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }
}